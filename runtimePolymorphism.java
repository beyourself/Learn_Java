class A{
        int a = 10;
        void amethod(){
            System.out.println("A method");
        }
}
    
class B extends A{
        int a = 20;
        void amethod(){
          System.out.println("B method"); 
        }
} 
public class RuntimePolymorphism{
    public static void main(String args[]){
        // create object of A class
        A a1 = new A();
        System.out.println("a value: "+a1.a);
        a1.amethod();

        A b1 = new B(); // upcasting
        System.out.println("a value: "+b1.a);
        b1.amethod();
        
        
    }
}

o/p-
a value: 10
A method
a value: 10
B method

this is example of overriding poytmorphism , this is also called as runtimePolymorphism.
in this we have used upcasting.
what is upcasting: - A superclass reference variable can refer to a subclass object.
This is also known as upcasting. Java uses this fact to resolve calls to overridden methods at run time.
here A is superclass and B is subclass.
n Java, we can override methods only, not the variables(data members), 
so runtime polymorphism cannot be achieved by data member.
Here we can see when we access b1.a then it gives superclass a variable value .